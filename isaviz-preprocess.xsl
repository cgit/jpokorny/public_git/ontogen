<!--
  shamelessly copied from
  http://en.wikipedia.org/wiki/Identity_transform#Using_XSLT
  -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:ex="http://purl.org/net/ns/ex#">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="owl:Ontology|ex:Example|comment()"/>
</xsl:stylesheet>
