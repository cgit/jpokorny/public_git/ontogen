<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/1999/xlink">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="@*[starts-with(., 'file:/')]">
    <xsl:attribute name="{name()}">
      <xsl:value-of select="concat('.#', substring-after(., '#'))"/>
    </xsl:attribute>
  </xsl:template>
  <xsl:template match="text()[starts-with(normalize-space(.), 'file:/')]">
    <xsl:variable name="space-prefix">
      <xsl:call-template name="iter-concat">
        <xsl:with-param name="itercat" select="'&#160;'"/>
        <xsl:with-param name="itercount"
                        select="floor(string-length(substring-before(., '#')) div 3 * 2)"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat($space-prefix, '#', substring-after(., '#'))"/>
  </xsl:template>

  <xsl:template name="iter-concat">
    <xsl:param name="itercat"/>
    <xsl:param name="itercount"/>
    <xsl:choose>
      <xsl:when test="$itercount = 0"/>
      <xsl:otherwise>
        <xsl:value-of select="$itercat"/>
        <xsl:call-template name="iter-concat">
          <xsl:with-param name="itercat" select="$itercat"/>
          <xsl:with-param name="itercount" select="$itercount - 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
