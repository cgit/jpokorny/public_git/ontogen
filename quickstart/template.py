#!/usr/bin/env python
# Example quickstart template for designing new ontologies
"""Descriptive vocabulary for foos

This vocabulary serves a purpose of shedding light into semantics
in the field of foos.
"""
__author__ = 'John Doe'


from sys import argv
#from genshi.input import XML

from ontogen.ontogen import Property, Class, Example, OntologyWithFigure, \
                            Ontologies

#from ontogen.ontogen import log
#log.setLevel('DEBUG')

base_uri = 'http://example.org/net/template'


#
# classes
#


# v0.1

class Foo(Class):
    """abstract foo item, use only its subclasses"""


class TFoo(Foo):
    """T-shaped foo

    Represents foos that are special because of the T-shape.
    """


#
# properties
#

# v0.1

@Foo.inDomainOf
class bar(Property):
    """plain bar

    Connects resources with foos.
    """
    range = "http://www.w3.org/2001/XMLSchema#string"


class baz(bar):
    """bar specialization known as baz"""


#
# examples
#


class ex_0_1(Example):
    """An example snippet.

<tmpl:tfoo about="#mytfoo">
  <tmpl:baz>test</tmpl:baz>
</tmpl:tfoo>
    """
    pfx = 'tmpl:'


#
# ontology + versions
#


ontologies = Ontologies()


class template(OntologyWithFigure):
    #creator = XML('''\
    #    <dc:X xmlns:dc="http://purl.org/dc/elements/1.1/"
    #    >John Doe</dc:X>''')
    pass


@ontologies.include
class template_0_1(template):
    version = '0.1'
    issued = '2013-02-19'
    modified = '2013-03-20'
    classes = [
        Foo,
        TFoo,
    ]
    properties = [
        bar,
        baz,
    ]
    examples = [
        ex_0_1,
    ]


#@ontologies.include
#@template_0_1.supersededBy
#class template_0_2(template):
#    ...


if __name__ == '__main__':
    ontologies.generate_latest(**(len(argv) > 1 and {'outfile': argv[1]} or {}))
